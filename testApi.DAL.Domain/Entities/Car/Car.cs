﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace testApi.DAL.Domain.Entities.Car
{
    [Table("Car")]
    public class Car : _BaseEntity
    {
        [Key]
        [Column("car_ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int carID { get; set; }

        [Column("car_name")]
        public string carName { get; set; }

        [Column("color")]
        public string color { get; set; }

        [Column("brand_ID")]
        public int? brandID { get; set; }

        [ForeignKey("brandID")]
        public virtual Brand brand { get; set; }

    }

    public class CarMap : IEntityTypeConfiguration<Car>
    {
        public void Configure(EntityTypeBuilder<Car> builder)
        {
            builder.HasOne(s => s.brand)
                .WithMany(d => d.car)
                .HasForeignKey(s => s.brandID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
