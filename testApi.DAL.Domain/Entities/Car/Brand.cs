﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace testApi.DAL.Domain.Entities.Car
{
    [Table("Brand")]
    public class Brand : _BaseEntity
    {
        [Key]
        [Column("brand_ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int brandID { get; set; }

        [Column("brand_name")]
        public string brandName { get; set; }

        public virtual ICollection<Car> car { get; set; }
    }
}
