﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace testApi.DAL.Domain.Entities
{
    public class _BaseEntity
    {
        public _BaseEntity()
        {
            createdDate = DateTime.Now;
            isEnabled = true;
            isDeleted = false;
        }

        [Column("CreatedDate")]
        public DateTime? createdDate { get; set; }

        [Column("CreatedBy")]
        public int? createdBy { get; set; }

        [Column("ModifiedDate")]
        public DateTime? modifiedDate { get; set; }

        [Column("ModifiedBy")]
        public int? modifiedBy { get; set; }

        [Column("IsEnabled")]
        public bool? isEnabled { get; set; }

        [Column("IsDeleted")]
        public bool? isDeleted { get; set; }

        [Column("DeletedDate")]
        public DateTime? deletedDate { get; set; }

        [Column("DeletedBy")]
        public int? deletedBy { get; set; }
    }
}
