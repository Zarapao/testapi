﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testApi.Business.Interfaces;
using testApi.Business.Models.Car;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace testApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarController : ControllerBase
    {
        private readonly ICarService _CarService;

        public CarController(ICarService CarService)
        {
            _CarService = CarService;

        }

        [HttpGet]
        [Route("getCar")]
        public ActionResult Get()
        {
            var result = _CarService.listCar();
            return Ok(result);
        }

        [HttpPost]
        [Route("addCar")]
        public ActionResult addCar([FromBody]CreateCarParameter input)
        {
            var result = _CarService.createCar(input);
            return Ok(result);
        }


    }
}
