﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testApi.Business.Interfaces;
using testApi.Business.Services;

namespace testApi.ServicesRegister
{
    public static class ServiceRegister
    {
        public static IServiceCollection RegisterBusiness(this IServiceCollection services)
        {
            services.AddScoped<ICarService, CarService>();

            return services;
        }
    }
}
