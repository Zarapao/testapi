using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testApi.Business.Interfaces;
using testApi.Business.Services;
using testApi.DAL.Interfaces;
using testApi.DAL.UnitOfWork;
using testApi.ServicesRegister;
using testAPI.DAL.Context;

namespace testApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<testApiContext>(option => option.UseMySql(Configuration.GetConnectionString("DBTestAPIConnection"),
                                        ServerVersion.Parse("10.6.4-MariaDB-1:10.6.4+maria~focal")));

            //Inject (Unit Of Work).
            services.AddScoped<IUnitOfWork, UnitOfWork<testApiContext>>();

            // Injection T2
            services.RegisterBusiness();

            services.AddControllers();

            services.AddCors(o => o.AddPolicy("testApiPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .WithOrigins()
                       .AllowAnyMethod()
                       .WithMethods()
                       .AllowAnyHeader()
                       .WithHeaders()
                       .SetIsOriginAllowedToAllowWildcardSubdomains();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(options => {
                options.AllowAnyOrigin();
                options.AllowAnyHeader();
                options.AllowAnyMethod();
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
