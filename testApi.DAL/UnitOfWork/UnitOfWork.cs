﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using testApi.DAL.Enums;
using testApi.DAL.Interfaces;
using testApi.DAL.Repository;

namespace testApi.DAL.UnitOfWork
{
    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext
    {
        private Dictionary<Type, object> _repositories;
        public ContextFactory ContxFactory { get; set; } = ContextFactory.testContext;
        public TContext Context { get; set; }
        public UnitOfWork(TContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public int SaveChanges()
        {
            return Context.SaveChanges();
        }
        public void Dispose()
        {
            Context?.Dispose();
        }
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (_repositories == null) _repositories = new Dictionary<Type, object>();

            var type = typeof(TEntity);
            if (!_repositories.ContainsKey(type)) _repositories[type] = new Repository<TEntity>(Context);

            return (IRepository<TEntity>)_repositories[type];
        }
        public IDbContextTransaction BeginTransaction()
        {
            return this.Context.Database.BeginTransaction();
        }
    }
}
