﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using testApi.DAL.Enums;

namespace testApi.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;

        ContextFactory ContxFactory { get; set; }

        int SaveChanges();

        IDbContextTransaction BeginTransaction();
    }

    public interface IUnitOfWork<TContext> : IUnitOfWork
            where TContext : DbContext
    {
        TContext Context { get; }
    }
}
