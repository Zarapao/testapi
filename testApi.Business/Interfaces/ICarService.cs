﻿using System;
using System.Collections.Generic;
using System.Text;
using testApi.Business.Common;
using testApi.Business.Models.Car;

namespace testApi.Business.Interfaces
{
    public interface ICarService
    {
        DataResult listCar();
        ActionReault createCar(CreateCarParameter input);
    }
}
