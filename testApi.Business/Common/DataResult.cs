﻿using System;
using System.Collections.Generic;
using System.Text;

namespace testApi.Business.Common
{
    public class DataResult
    {
        public bool status { get; set; }
        public string message { get; set; }
        public dynamic data { get; set; }
    }
}
