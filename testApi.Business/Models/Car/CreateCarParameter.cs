﻿using System;
using System.Collections.Generic;
using System.Text;

namespace testApi.Business.Models.Car
{
    public class CreateCarParameter
    {
        public string carName { get; set; }
        public int brandId { get; set; }
        public string color { get; set; }
    }
}
