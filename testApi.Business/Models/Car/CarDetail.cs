﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace testApi.Business.Models.Car
{
    public class CarDetail
    {
        public int carId { get; set; }
        public string brand { get; set; }
        public string model { get; set; }
    }
}
