﻿using System;
using System.Collections.Generic;
using System.Text;
using testApi.Business.Common;
using testApi.Business.Interfaces;
using testApi.Business.Models.Car;
using testApi.DAL.Domain.Entities.Car;
using testApi.DAL.Interfaces;

namespace testApi.Business.Services
{
    public class CarService : ICarService
    {
        private readonly IUnitOfWork _UnitOfWork;
        public CarService(IUnitOfWork UnitOfWork)
        {
            _UnitOfWork = UnitOfWork;
        }

        public DataResult listCar()
        {
            DataResult result = new DataResult();

            result.status = true;
            result.message = "Get data success.";
            result.data = new CarDetail
            {
               carId = 1,
               model = "Honda",
               brand = "Civic"
            };

            return result;
        }

        public ActionReault createCar(CreateCarParameter input)
        {
            ActionReault result = new ActionReault();

            using (var transaction = _UnitOfWork.BeginTransaction())
            {
                try
                {
                    var repoCar = _UnitOfWork.GetRepository<Car>();

                    repoCar.Add(new Car { 
                        carName = input.carName,
                        brandID = input.brandId,
                        color = input.color
                    });

                    _UnitOfWork.SaveChanges();


                    transaction.Commit();

                    result.status = true;
                    result.message = "Create car success.";

                    return result;
                }
                catch (Exception ex) {

                    transaction.Rollback();
                    result.status = false;
                    result.message = "Create car fail : " + ex.Message;

                    return result; 
                }
            }

                

        }
    }
}
