﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace testAPI.DAL.Context.Migrations
{
    public partial class AddSeeder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Brand",
                columns: new[] { "brand_ID", "brand_name", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "IsDeleted", "IsEnabled", "ModifiedBy", "ModifiedDate" },
                values: new object[] { 1, "Honda", null, new DateTime(2021, 10, 8, 15, 58, 17, 853, DateTimeKind.Local).AddTicks(4813), null, null, false, true, null, null });

            migrationBuilder.InsertData(
                table: "Brand",
                columns: new[] { "brand_ID", "brand_name", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "IsDeleted", "IsEnabled", "ModifiedBy", "ModifiedDate" },
                values: new object[] { 2, "Toyota", null, new DateTime(2021, 10, 8, 15, 58, 17, 854, DateTimeKind.Local).AddTicks(5187), null, null, false, true, null, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Brand",
                keyColumn: "brand_ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Brand",
                keyColumn: "brand_ID",
                keyValue: 2);
        }
    }
}
