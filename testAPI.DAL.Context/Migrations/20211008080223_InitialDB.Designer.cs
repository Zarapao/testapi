﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using testAPI.DAL.Context;

namespace testAPI.DAL.Context.Migrations
{
    [DbContext(typeof(testApiContext))]
    [Migration("20211008080223_InitialDB")]
    partial class InitialDB
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 64)
                .HasAnnotation("ProductVersion", "5.0.10");

            modelBuilder.Entity("testApi.DAL.Domain.Entities.Car.Brand", b =>
                {
                    b.Property<int>("brandID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("brand_ID");

                    b.Property<string>("brandName")
                        .HasColumnType("longtext")
                        .HasColumnName("brand_name");

                    b.HasKey("brandID");

                    b.ToTable("Brand");
                });

            modelBuilder.Entity("testApi.DAL.Domain.Entities.Car.Car", b =>
                {
                    b.Property<int>("carID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("car_ID");

                    b.Property<int?>("brandID")
                        .HasColumnType("int")
                        .HasColumnName("brand_ID");

                    b.Property<string>("carName")
                        .HasColumnType("longtext")
                        .HasColumnName("car_name");

                    b.Property<string>("color")
                        .HasColumnType("longtext")
                        .HasColumnName("color");

                    b.Property<int?>("createdBy")
                        .HasColumnType("int")
                        .HasColumnName("CreatedBy");

                    b.Property<DateTime?>("createdDate")
                        .HasColumnType("datetime(6)")
                        .HasColumnName("CreatedDate");

                    b.Property<int?>("deletedBy")
                        .HasColumnType("int")
                        .HasColumnName("DeletedBy");

                    b.Property<DateTime?>("deletedDate")
                        .HasColumnType("datetime(6)")
                        .HasColumnName("DeletedDate");

                    b.Property<bool?>("isDeleted")
                        .HasColumnType("tinyint(1)")
                        .HasColumnName("IsDeleted");

                    b.Property<bool?>("isEnabled")
                        .HasColumnType("tinyint(1)")
                        .HasColumnName("IsEnabled");

                    b.Property<int?>("modifiedBy")
                        .HasColumnType("int")
                        .HasColumnName("ModifiedBy");

                    b.Property<DateTime?>("modifiedDate")
                        .HasColumnType("datetime(6)")
                        .HasColumnName("ModifiedDate");

                    b.HasKey("carID");

                    b.HasIndex("brandID");

                    b.ToTable("Car");
                });

            modelBuilder.Entity("testApi.DAL.Domain.Entities.Car.Car", b =>
                {
                    b.HasOne("testApi.DAL.Domain.Entities.Car.Brand", "brand")
                        .WithMany("car")
                        .HasForeignKey("brandID");

                    b.Navigation("brand");
                });

            modelBuilder.Entity("testApi.DAL.Domain.Entities.Car.Brand", b =>
                {
                    b.Navigation("car");
                });
#pragma warning restore 612, 618
        }
    }
}
