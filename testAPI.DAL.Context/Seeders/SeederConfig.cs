﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using testApi.DAL.Domain.Entities.Car;

namespace testAPI.DAL.Context.Seeders
{
    public class SeederConfig
    {
        public static void SeedConfig(ModelBuilder ModelBuilder)
        {
            ModelBuilder.Entity<Brand>().HasData(
            new Brand
            {
                brandID = 1,
                brandName = "Honda"
            }, new Brand
            {
                brandID = 2,
                brandName = "Toyota"
            });

        }
    }
}
