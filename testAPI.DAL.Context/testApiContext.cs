﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using testApi.DAL.Domain.Entities.Car;

namespace testAPI.DAL.Context
{
    public class testApiContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Brand> Brands { get; set; }

        protected override void OnModelCreating(ModelBuilder ModelBuilder)
        {
            Seeders.SeederConfig.SeedConfig(ModelBuilder);
            base.OnModelCreating(ModelBuilder);
        }

        public testApiContext(DbContextOptions<testApiContext> options) : base(options)
        {
        }
    }

    public class DesignTDbContextFactory : IDesignTimeDbContextFactory<testApiContext>
    {
        public testApiContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                                                                         .AddJsonFile(@Directory.GetCurrentDirectory() + "/../testApi/appsettings.json")
                                                                         .Build();
            var builder = new DbContextOptionsBuilder<testApiContext>();
            var connectionString = configuration.GetConnectionString("DBTestAPIConnection");
            builder.UseMySql(connectionString, ServerVersion.Parse("10.6.4-MariaDB-1:10.6.4+maria~focal"));
            return new testApiContext(builder.Options);

        }

    }
}
